<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <form>
            <legend class="capital-heading"><?= __('Add article') ?></legend>
            <div class="form-group">
                <label>Select journal</label>
                <select id="add-journal-typeselect" class="form-control">
                <option>CDLJ</option>
                <option>CDLP</option>
                <option>CDLB</option>
                <option>CDLN</option>
                </select>
                <button onclick="OnTypeSelectSubmit()" type="button" class="btn btn-primary">Add</button>
            </div>
        </form>
    </div>
    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <a  href='/admin/articles/link/publications' style="color:white" type="button" class="btn btn-primary">Link articles and publications</a>
    </div>

</div>

<!-- Page script for admin/journals/add -->
<script>
function OnTypeSelectSubmit() {
    var type = $('#add-journal-typeselect').val();
    window.location.href='/admin/articles/add/' + type;
}
</script>