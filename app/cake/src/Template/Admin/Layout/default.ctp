<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */

// Get  Roles of Authenticated User
$roles = $this->Session->read('Auth.User.roles');
$name = $this->Session->read('Auth.User.username');


// Check if role_id 1 or 2 is present
$ifRoleExists = !is_null($roles) ?  array_intersect($roles, [1, 2]) : [];

$ifRoleExists = !empty($ifRoleExists) ? 1 : 0;

?>


<!DOCTYPE html>
<html lang="en-US">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?> - Cuneiform Digital Library Initiative
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('main.css')?>
    <?= $this->Html->css('font-awesome/css/font-awesome.min.css')?>

    <?php
        // For using Bootstrap dropdowns, set variable $includePopper in your view
        if (isset($includePopper) && $includePopper) {
            echo $this->Html->script('popper.min.js');
        }
    ?>

    <?= $this->Html->script('jquery.min.js')?>
    <?= $this->Html->script('bootstrap.min.js')?>
    <?= $this->Html->script('js.js')?>

    <?= $this->element('google-analytics'); ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <nav class="navbar navbar-expand navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <?php if ($ifRoleExists) { ?>
                    <li class="nav-item mx-3">
                        <?= $this->Html->link(in_array(1, $this->Session->read('Auth.User.roles')) ? "Admin Dashboard" : "Dashboard", '/admin/dashboard', ['class' => 'nav-link']) ?>
                    </li>
                <?php } ?>

               <li class="publication-nav nav-item mx-3 dropdown">
					<a class="nav-link dropdown-toggle"  role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Publications
					</a>
					<div class="dropdown-menu" aria-labelledby="">
						<a class="dropdown-item" href="/admin/articles/cdlj"> Cuneiform Digital Library Journal</a>
                        <a class="dropdown-item" href="/admin/articles/cdln">Cuneiform Digital Library Notes</a>
                        <a class="dropdown-item" href="/admin/articles/cdlp">Cuneiform Digital Library Preprint</a>
                        <hr>
                        <a class="dropdown-item" href="/admin/articles/add">Add articles</a>
					</div>
				</li>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">Resources</a>
                </li>
                <li class="publication-nav nav-item mx-3 dropdown">
					<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?php echo $name ?>
					</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                       <?= $this->Html->link("My Profile", [
                        'controller' => 'Users',
                        'action' => 'profile',
                        'prefix' => false
                        ], [
                        'class' => 'dropdown-item'
                        ]) ?>
						<a class="dropdown-item" href="/admin/artifacts"> Manage Artifacts</a>
                        <a class="dropdown-item" href="/admin/articles/cdlb">Manage Authors</a>
                        <a class="dropdown-item" href="/admin/publications">Manage Publications</a>
                        <a class="dropdown-item" href="/admin/articles/cdlj">Manage CDL Journals</a>
                        <hr>
                        <?= $this->Html-> link('Logout', [
                            'controller' => 'Logout',
                            'action' => 'index',
                            'prefix' => false
                        ], [
                            'class' => 'dropdown-item'
                            ]
                        ); ?>
                       
                        
					</div>
				</li>
               
                
            </ul>
        </div>
    </nav>

    <nav class="navbar navbar-expand-lg navbar-light bg-transparent py-0 my-5" id="navbar-main">
        <a class="navbar-brand logo py-0 ml-5" href="/">
            <div class="navbar-logo">
                <img src="/images/logo.png" class="d-none d-xl-block" alt="cdli-logo" />
                <img src="/images/logo-no-text.svg" class="d-xl-none" alt="cdli-logo" />				
            </div>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-3 active">
                    <?= $this->Html->link("Browse", '/browse', ['class' => 'nav-link']) ?>
                </li>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">Contribute</a>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item mx-3 dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/">Search</a>
                        <a class="dropdown-item" href="/advancedsearch">Advanced search</a>
                        <a class="dropdown-item" href="/SearchSettings">Search settings</a>
                        <a class="dropdown-item" href="/cqp4rdf">Corpus search</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid text-center contentWrapper">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>

	<noscript>
		<p class="alert alert-danger alert-dismissible fade show textcenter">Your browser does not support javascript please enable it for better experince!</p>
	</noscript>

    <footer>
		<div class="container">
			<div>
				<div class="row footer-1 py-5">
					<div class="col-lg-3 d-none d-lg-block">
						<h2 class="heading">Navigate</h2>
						<p><a href="/browse">Browse collection</a></p>
						<p><a href="#">Contribute</a></p>
						<p><a href="#">About CDLI</a></p>
						<p><a href="#">Search collection</a></p>
					</div>
					<div class="col-md-6 col-lg-4">
						<h2 class="heading">Acknowledgement</h2>
                        <p class="backers">
                            Support for this initiative has been generously provided by the 
							<a href="https://mellon.org/" target="_blank">Mellon Foundation</a>, 
                            the <a href="https://www.nsf.gov/" target="_blank">NSF</a>, 
							the <a href="https://www.neh.gov/" target="_blank">NEH</a>, 
							the <a href="https://www.imls.gov/" target="_blank">IMLS</a>, 
							the <a href="https://www.mpg.de/en" target="_blank">MPS</a>, 
							<a href="http://www.ox.ac.uk/" target="_blank">Oxford University</a> 
							and <a href="http://www.ucla.edu/" target="_blank">UCLA</a>, 
							with additional support
                            from <a href="https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx" target="_blank">SSHRC</a> and 
							the <a href="https://www.dfg.de/" target="_blank">DFG</a>. 
							Computational resources and network services are provided by 
							<a href="https://humtech.ucla.edu/" target="_blank">UCLA’s HumTech</a>,
                            <a href="https://www.mpiwg-berlin.mpg.de/" target="_blank">MPIWG</a>, 
							and <a href="https://www.computecanada.ca/" target="_blank">Compute Canada</a>.
                        </p>
					</div>
                    <div class="col-lg-1 d-none d-lg-flex"></div>
					<div class="col-md-6 col-lg-4 contact">
						<h2 class="heading">Contact Us</h2>
						<p class="p">
                            <li style="list-style-type : none">Cuneiform Digital Library Initiative</li>
							<li style="list-style-type : none">Linton Rd, Oxford OX2 6UD</li>
							<li style="list-style-type : none">United Kingdom</li>
						</p>
						<div class="d-flex">
							<div class="twitter">
								<a href="https://twitter.com/cdli_news" target="_blank">
									<span class="fa fa-twitter"></span>
								</a>
							</div>
							<div class="mail">
								<a href="mailto:cdli@ucla.edu" target="_blank">
									<span class="fa fa-envelope fa-3x"></span>
								</a>
							</div>
							<div> <a href="https://giving.ucla.edu/Standard/NetDonate.aspx?SiteNum=245" class="btn donate" role="button" target="_blank">Donate</a></div>
						</div>
						<!-- <a
						href="https://www.w3.org/WAI/WCAG2AAA-Conformance"
						title="Explanation of WCAG 2.0 Level Triple-A Conformance">
						<img
						height="32"
						width="88"
						src="https://www.w3.org/WAI/wcag2AAA-blue"
						alt="Level Triple-A conformance, W3C WAI Web Content Accessibility Guidelines 2.0"
						/> -->
					</a>
				</div>
			</div>
                <!-- <div class="footer-2">
                    <a href="https://mellon.org/" target="_blank"><img class="img" src="/images/logos/m.png" alt="Mellon Foundation logo"></a>
                    <a href="https://www.neh.gov/" target="_blank"><img class="img" src="/images/logos/neh.png" alt="NEFTH logo"></a>
                    <a href="http://www.ucla.edu/" target="_blank"><img class="img" src="/images/logos/ucla.ico" alt="University of California logo"></a>
                    <a href="https://humtech.ucla.edu/" target="_blank"><img class="img" src="/images/logos/humtech.png" alt="Humtech logo"></a>
                    <a href="https://humanities.ucla.edu/" target="_blank"><img class="img" src="/images/logos/uclahum.png" alt="University of California human png"></a>
                    <a href="https://www.computecanada.ca/" target="_blank"><img class="img" src="/images/logos/computecanada.png" alt="computecanada logo"></a>
                    <a href="http://www.ox.ac.uk/" target="_blank"><img class="img" src="/images/logos/university-oxford.ico" alt="University oxford logo"></a>
                    <a href="https://www.utoronto.ca/" target="_blank"><img class="img" src="/images/logos/university-tornato.ico" alt="Toronto Logo"></a>
                    <a href="https://www.dfg.de/" target="_blank"><img class="img" src="/images/logos/dfg.png" alt="DFG logo"></a>
                    <a href="https://www.mpiwg-berlin.mpg.de/" target="_blank"><img class="img" src="/images/logos/mm.png" alt="MPIWG logo"></a>
				    <a href="https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx" target="_blank"><img class="img" src="/images/logos/sshrc_logo.jpeg" alt="SSHRC logo"></a>
			    </div> -->
		    </div>
	    </div>
        </div>
        <div class="footer-end p-4">
	        <div class="text-center text-white">
		        © 2019-2020 Cuneiform Digital Library Initiative.
	        </div>
        </div>
    </footer>
</body>

</html>
