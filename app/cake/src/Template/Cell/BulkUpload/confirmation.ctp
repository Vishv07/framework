<h1 class="display-3 header-txt text-left"><?= __('Bulk Upload '.preg_replace('/(?<!^)[A-Z]/', '-$0', $table)) ?></h1>
<?php if (isset($entities) and isset($error_list_validation)): ?>
    <?= $this->Form->create(null, ['url' => ['action' => 'add/bulk']]) ?>
        <?= $this->Form->input('entities_serialize', ['type' => 'hidden', 'value' => base64_encode(serialize($entities))]) ?>
        <?= $this->Form->input('header_serialize', ['type' => 'hidden', 'value' => base64_encode(serialize($header))]) ?>
        <div class="justify-content-md-center row">
            <table>
                <tr>
                    <td><?= $this->Form->submit('Proceed', ['class' => 'btn cdli-btn-blue']); ?>
                    </td>
                    <td><?= $this->Html->link('Cancel', [
                        'action' => 'add', 'bulk'],
                        ['class' => 'btn cdli-btn-light']) ?>
                    </td>
                </tr>
            </table>
        </div>
    <?= $this->Form->end() ?>
    
    <h3 class="display-4 pt-3 text-left"><?= __('Validation Error Report') ?></h3>
    <div class="table-responsive">
        <table class="table-bootstrap my-3 mx-0">
            <thead>
                <tr>
                    <th scope="col"><?= __('Line No.') ?></th>
                    <th scope="col" colspan=<?= count($header) ?>><?= __('Data') ?></th>
                    <th scope="col"><?= __('Errors') ?></th>
                </tr>
                <tr>
                    <th scope="col"></th>
                    <?php foreach ($header as $field): ?>
                        <th scope="col"><?= h($field) ?></th> 
                    <?php endforeach ?>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($error_list_validation as $error): ?>
                <tr>
                    <td> <?= h($error[0]) ?></td>
                    <?php foreach (array_combine($header,explode('","', $error[1])) as $value): ?>
                            <td><?= __($value) ?></td> 
                    <?php endforeach ?>
                    <td nowrap="nowrap">
                        <?php foreach ($error[2] as $row): ?>
                            <?= h($row) ?><br>
                        <?php endforeach; ?>
                        <?php if(isset($error[3])): ?>
                                <?= $this->Html->link('(Edit Link)', ['controller' => $table, 'action' => 'edit', $error[3]], ['target' => '_blank']) ?>
                        <?php endif ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
