<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AgadeMails Model
 *
 * @property \App\Model\Table\CdliTagsTable|\Cake\ORM\Association\BelongsToMany $CdliTags
 *
 * @method \App\Model\Entity\AgadeMail get($primaryKey, $options = [])
 * @method \App\Model\Entity\AgadeMail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AgadeMail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AgadeMail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AgadeMail|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AgadeMail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AgadeMail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AgadeMail findOrCreate($search, callable $callback = null, $options = [])
 */
class AgadeMailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('agade_mails');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsToMany('CdliTags', [
            'foreignKey' => 'agade_mail_id',
            'targetForeignKey' => 'cdli_tag_id',
            'joinTable' => 'agade_mails_cdli_tags'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 400)
            ->allowEmpty('title');

        $validator
            ->dateTime('date')
            ->allowEmpty('date');

        $validator
            ->scalar('category')
            ->maxLength('category', 100)
            ->allowEmpty('category');

        $validator
            ->scalar('content')
            ->maxLength('content', 16777215)
            ->allowEmpty('content');

        $validator
            ->boolean('is_public')
            ->allowEmpty('is_public');

        return $validator;
    }
}
